/**
 * CƠ SỞ DỮ LIỆU QUẢN LÝ DANH SÁCH Drink
*/

// Khởi tạo mảng gDrinkDb gán dữ liêu từ server trả về 
var gDrinkDb = {
  drinks: [],

  // order methods
  // function get Drink index form Drink id
  // get Drink index from Drink id
  getIndexFromDrinkId: function (paramDrinkId) {
    var vDrinkIndex = -1;
    var vDrinkFound = false;
    var vLoopIndex = 0;
    while (!vDrinkFound && vLoopIndex < this.drinks.length) {
      if (this.drinks[vLoopIndex].id === paramDrinkId) {
        vDrinkIndex = vLoopIndex;
        vDrinkFound = true;
      }
      else {
        vLoopIndex++;
      }
    }
    return vDrinkIndex;
  },

  // hàm show order obj lên form
  showDrinkDataToForm: function (paramRequestData) {
   
    $("#inp-ma-nuoc-uong").val(paramRequestData.maNuocUong);
    $("#inp-ten-nuoc-uong").val(paramRequestData.tenNuocUong);
    $("#inp-don-gia").val(paramRequestData.donGia);
    $("#inp-ghi-chu").val(paramRequestData.ghiChu);
    $("#inp-ngay-tao").val(paramRequestData.ngayTao);
    $("#inp-ngay-cap-nhat").val(paramRequestData.ngayCapNhat);
  },
  
};

